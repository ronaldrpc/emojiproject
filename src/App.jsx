import { useEffect, useState } from "react";
import "./App.css";

import "bootstrap/dist/css/bootstrap.min.css";

import { Title } from "./components/Title";
import { Search } from "./components/Search";
import { EmojiList } from "./components/EmojiList";
import Pagination from "react-js-pagination";

let LIMIT = 24;
let URL = `http://localhost:3001/emojis`;

function App() {
  const [items, setItems] = useState([]);
  const [filter, setFilter] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  // console.log(items);

  useEffect(() => {
    fetch(URL) // `${URL}?_limit=${LIMIT}&_page=${currentPage}`
      .then((response) => response.json())
      .then((datos) => {
        let lim_sup = currentPage * LIMIT;
        let lim_inf = lim_sup - LIMIT;
        let res = filterData(datos);

        let emojis = res.slice(lim_inf, lim_sup);
        setItems(emojis);
      });
  }, [currentPage, filter]);

  useEffect(() => {
    let inputSearch = document.getElementById("buscarEmoji");
    inputSearch.addEventListener("keyup", (event) => {
      setFilter(event.target.value);
    });
  }, [filter]);

  function filterData(data) {
    if (filter) {
      return data.filter((item) =>
        item.title.toLowerCase().includes(filter.toLowerCase())
      );
    }
    return data;
  }

  function onChangePage(page) {
    setCurrentPage(page);
  }

  // TODO: add pagination from react-js-pagination
  // TODO: install bootstrap and remove link from index.html
  // TODO: fix pagination filtering

  return (
    <div className="container">
      <Title />
      <Search />
      <EmojiList data={filterData(items)} />
      <div className="d-flex justify-content-center">
        <Pagination
          itemClass="page-item"
          linkClass="page-link"
          activePage={currentPage}
          itemsCountPerPage={LIMIT}
          totalItemsCount={1820 / 24}
          onChange={onChangePage}
        />
      </div>
    </div>
  );
}

export default App;
