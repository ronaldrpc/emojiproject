import {EmojiCard} from './EmojiCard'

export function EmojiList({data}) {

  let emojiLista = data.map((emoji) => {
    return (
      <EmojiCard
        key={emoji.title} // esto es para que no de error de unique key
        title={emoji.title}
        symbol={emoji.symbol}
        keywords={emoji.keywords} />
    );
  });

  return (
    <div className="row">
      {emojiLista}
    </div>
  );
}