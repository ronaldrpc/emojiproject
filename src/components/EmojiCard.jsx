export function EmojiCard(emojiInfo) {
  return (
    <div className="col-sm-3 mb-3">
      <div className="card" >
        <div className="card-body text-center">
          <h4 className="card-title"> {emojiInfo.symbol} </h4>
          <h6 className="card-subtitle mb-2 text-muted"> {emojiInfo.title}</h6>
          <p className="card-text"> {emojiInfo.keywords} </p>
        </div>
      </div>
    </div>
  );
}